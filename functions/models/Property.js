const mongoose = require('mongoose');
const Schema = mongoose.Schema;

//create user schema
SALT_WORK_FACTOR = 10;
const  PropertySchema = new Schema({
    name:{
        type:String,
        required:[ true,'name field is required']

    },

    agentID:{
        type:String,
        required:[ true,'agentID field is required']

    },
    abb:{
        type:String,
        required:[ true,'abb field is required']

    },
    contactnumber:{
        type:String,
        required:[ true,'contactnumber field is required']

    },
 
    address1:{
        type:String,
 

    },
 
   suburb:{
        type:String,
        required:[ true,'surburb field is required']

    },
    city:{
        type:String,
        required:[ true,'city field is required']

    },

    price:{
        type:Number,
        required:[ true,'price field is required']

    },

    pricepertoken:{
        type:Number,
        required:[ true,'pricepertoken field is required']

    },
    province:{
        type:String,
        required:[ true,'province field is required']

    },
propertyType:{
        type:String,
        required:[ true,'propertyType field is required']

    },

    sellingType:{
        type:String,
        required:[ true,'sellingType field is required']

    },
    size:{
        type:Number,
      
        required:[ true,'size field is required']

    },

    suburb:{
        type:String,
      
        required:[ true,'size field is required']

    },

    tokens:{
        type:Number,
      
        required:[ true,'tokens field is required']

    },
    value:{
        type:Number,
      
        required:[ true,'value field is required']

    },


});


const property = mongoose.model('Properties',PropertySchema);
module.exports=property;