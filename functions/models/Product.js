const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const bcrypt = require('bcrypt');
//create user schema
SALT_WORK_FACTOR = 10;
const  ProductSchema = new Schema({
    name:{
        type:String,
        required:[ true,'name field is required']

    },

    price:{
        type:String,
        required:[ true,'price field is required']

    },
    country:{
        type:String,
        required:[ true,'country field is required']

    },

type:{
        type:String,
        required:[ true,'type field is required']

    },
    type2:{
        type:String,
        required:[ true,'type2 field is required']

    },
    bio:{
        type:String,
        required:[ true,'bio field is required']

    },

    colors:{
        type:Array,
        required:[ true,' images field is required']

    },
    images:{
        type:Array,
        required:[ true,' images field is required']

    },
    status:{
        type:String,
 

    },

 
  


});

;


const Products = mongoose.model('Products',ProductSchema);
module.exports=Products;