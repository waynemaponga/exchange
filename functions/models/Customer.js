const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const bcrypt = require('bcrypt');
const shortid = require('shortid');
//create user schema
SALT_WORK_FACTOR = 10;
const CustomerSchema = new Schema({
    username:{
        type:String,
        unique:true,
        required:[true,'Name field is required']

    },
 
    status:{
        type:String,
        
        default:'registered',
        

    },
    country:{
        type:String,
        
   
        

    },
    number:{
        type:String,
        
   
        

    },
    code:{
        type:String,
        default:shortid.generate(),
        
   
        

    },

    password:{
        type:String,
        required:[true,'password field is required']
    },



});

CustomerSchema.pre('save', function(next) {
    var user = this;

    // only hash the password if it has been modified (or is new)
    if (!user.isModified('password')) return next();

    // generate a salt
    bcrypt.genSalt(SALT_WORK_FACTOR, function(err, salt) {
        if (err) return next(err);

        // hash the password using our new salt
        bcrypt.hash(user.password, salt, function(err, hash) {
            if (err) return next(err);

            // override the cleartext password with the hashed one
            user.password = hash;
            next();
        });
    });
});

CustomerSchema.methods.comparePassword = function(candidatePassword, cb) {
    bcrypt.compare(candidatePassword, this.password, function(err, isMatch) {
        if (err) return cb(err);
        cb(null, isMatch);
    });
};

const Customer = mongoose.model('Customers',CustomerSchema);
module.exports=Customer;