const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const bcrypt = require('bcrypt');
//create user schema
SALT_WORK_FACTOR = 10;
const  BookingsSchema = new Schema({
    propertyID:{
        type:String,
        required:[ true,'PropertyID field is required']

    },
    propertyName:{
        type:String,
        required:[ true,'PropertName field is required']

    },
    ContactNumber:{
        type:String,
        required:[ true,'ContactNumber field is required']

    },

date:{
        type:Date,
        required:[ true,'dateTime field is required']

    },
    time:{
        type:String,
        required:[ true,'Time field is required']

    },
    firstname:{
        type:String,
        required:[ true,'firstname field is required']

    },
    lastname:{
        type:String,
        required:[ true,'lastname field is required']

    },
    email:{
        type:String,
        required:[ true,'email field is required']

    },
  

 
  


});

;


const Bookings = mongoose.model('Bookings',BookingsSchema);
module.exports=Bookings;