const mongoose = require('mongoose');
const Schema = mongoose.Schema;

//create user schema
SALT_WORK_FACTOR = 10;
const  PropertyPixSchema = new Schema({
   PropertyID:{
        type:String,
        required:[ true,'PropertyID field is required']

    },

    
    images:{
        type:Array,
        required:[ true,' images field is required']

    },

    created_at: {type: Date, default: Date.now}


});


const propertypix = mongoose.model('Propertiespix',PropertyPixSchema);
module.exports=propertypix;