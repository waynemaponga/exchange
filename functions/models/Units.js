const mongoose = require('mongoose');
const Schema = mongoose.Schema;

//create user schema
SALT_WORK_FACTOR = 10;
const  UnitsSchema = new Schema({
    name:{
        type:String,
        required:[ true,'name field is required']

    },

    number:{
        type:String,
        required:[ true,'number field is required']

    },
 
    type:{
        type:String,
        required:[ true,'type field is required']

    },
    PropertyID:{
        type:String,
        required:[ true,'PropertyID field is required']

    },
    nameProperty:{
        type:String,
        required:[ true,'nameProperty field is required']

    },
 

    size:{
        type:String,
      
        required:[ true,'size field is required']

    },
      users:[String],
 
    created_at: {type: Date, default: Date.now}

});


const Units = mongoose.model('Units',UnitsSchema);
module.exports=Units;