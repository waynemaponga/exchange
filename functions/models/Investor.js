const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const bcrypt = require('bcrypt');
//create user schema
SALT_WORK_FACTOR = 10;
const  InvestorSchema = new Schema({
    firstname:{
        type:String,
        required:[ true,'firstname field is required']

    },

    
    lastname:{
        type:String,
        required:[ true,'lastname field is required']

    },
    contactnumber:{
        type:String,
        required:[ true,'contactnumber field is required']

    },
    email:{
        type:String,
        unique:true,
        required:[ true,'email field is required']

    },
    address1:{
        type:String,
 

    },
    address2:{
        type:String,
    

    },
 
   suburb:{
        type:String,
        required:[ true,'surburb field is required']

    },
    city:{
        type:String,
        required:[ true,'city field is required']

    },
    province:{
        type:String,
        required:[ true,'province field is required']

    },
    status:{
        type:String,
        default:"0",
        required:[ true,'status field is required']

    },
    password:{
        type:String,
        required:[ true,'password field is required']

    },


});

InvestorSchema.pre('save', function(next) {
    var user = this;

    // only hash the password if it has been modified (or is new)
    if (!user.isModified('password')) return next();

    // generate a salt
    bcrypt.genSalt(SALT_WORK_FACTOR, function(err, salt) {
        if (err) return next(err);

        // hash the password using our new salt
        bcrypt.hash(user.password, salt, function(err, hash) {
            if (err) return next(err);

            // override the cleartext password with the hashed one
            user.password = hash;
            next();
        });
    });
});

InvestorSchema.methods.comparePassword = function(candidatePassword, cb) {
    bcrypt.compare(candidatePassword, this.password, function(err, isMatch) {
        if (err) return cb(err);
        cb(null, isMatch);
    });
};

const investors = mongoose.model('investors',InvestorSchema);
module.exports=investors;