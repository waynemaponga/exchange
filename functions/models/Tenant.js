const mongoose = require('mongoose');
const Schema = mongoose.Schema;

//create user schema
SALT_WORK_FACTOR = 10;
const  TenantSchema = new Schema({
    name:{
        type:String,
        required:[ true,'name field is required']

    },

    lastname:{
        type:String,
        required:[ true,'lastname field is required']

    },
 
    contactnumber1:{
        type:String,
        required:[ true,'contactnumber field is required']

    },
    contactnumber2:{
        type:String,
        

    },
 
    email:{
        type:String,
        required:[ true,'email field is required']

    },

    unitID:{
        type:String,
        required:[ true,'unitID field is required']

    }, uintName:{
        type:String,
        required:[ true,'uintName field is required']

    },
    PropertyID:{
        type:String,
        required:[ true,'PropertyID field is required']

    },
    nameProperty:{
        type:String,
        required:[ true,'nameProperty field is required']

    },

    gender:{
        type:String,
      
        required:[ true,'gender field is required']

    },
    kincontactnumber:{
        type:String,
      
        required:[ true,'kincontactnumber field is required']

    },
    kinemail:{
        type:String,
      
        required:[ true,'kinemail field is required']

    },
 
    kinfirstname:{
        type:String,
      
        required:[ true,'kinfirstname field is required']

    },
    kinlastname:{
        type:String,
      
        required:[ true,'kinlastname field is required']

    },

});


const tenant = mongoose.model('Tenants',TenantSchema);
module.exports=tenant;