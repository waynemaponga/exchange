const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const shortid = require('shortid');
//create user schema
SALT_WORK_FACTOR = 10;
//create user schema
const  CommSchema = new Schema({
    name:{
        type:String,
        required:[true,'name field is required']
    },
    Address:{
        type:String,
        required:[true,'Address field is required']
    },
    Presenter :{
        type:String,
        required:[true,'Identitynumber field is required']
    },
     ID :{
        type:String,
        required:[true,'ID field is required']
    },
    Nationality :{
        type:String,
        required:[true,'Nationality field is required']
    },
    MemberID :{
        type:String,
        default:shortid.generate(),
        unique:true,
        required:[true,'MemberID field is required']
    },
    Contactnumber:{
        type:String,
        required:[false,'Contactnumber field is required']
    },
  
    About:{
        type:String,
        required:[false,'About field is required']
    },
      
    status:{ 
        type:String,
        required:[true,'About field is required']
    },
    email:{
        type:String,
        unique:true,
        required:[true,'email field is required']
    },
    netype:{
        type:String,
        required:[true,'name field is required']
    },
});


const User = mongoose.model('Comm',CommSchema);
module.exports=User;
