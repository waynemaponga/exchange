const mongoose = require('mongoose');
const Schema = mongoose.Schema;
//create user schema
SALT_WORK_FACTOR = 10;
const  ProfileSchema = new Schema({
    about:{
        type:String,
        require:true,
        required:['about field is required']

    },
    name:{
        type:String,
        unique:true,
        require:true,
        required:['name field is required']

    },
    logo:{
        type:String,
        require:true,
        required:['logo field is required']

    },
    username:{
        type:String,
        require:true,
        required:['username field is required']

    },
    ig:{
        type:String,
        required:['ig field is required']

    },
    fb:{
        type:String,
        required:['fb field is required']

    },
    number:{
        type:String,
        require:true,
        required:['number field is required']

    },
    key:{
        type:String,
        require:true,
        required:['key field is required']

    },
    date:{
        type: String, 
        required:[false,'date field is required']
    },

});



const Profile = mongoose.model('profile',ProfileSchema);
module.exports=Profile;