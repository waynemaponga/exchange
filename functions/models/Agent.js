const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const shortid = require('shortid');
//create user schema
SALT_WORK_FACTOR = 10;
const  AgentSchema = new Schema({
    name:{
        type:String,
    
        required:['Name field is required']

    },
    lastname:{
        type:String,
    
        required:['lastname field is required']

    },
    ID:{
        type:String,
        unique:true,
        'default': shortid.generate,
        required:[ true,'Name field is required']

    },
    contactnumber:{
        type:String,
  
        required:['booknumber field is required']

    },

    email:{
        type:String,
  
        required:['pic field is required']

    },
  
    companyname:{
        type:String,
  
        required:['companyname field is required']

    },




});



const Agents = mongoose.model('Agents',AgentSchema);
module.exports=Agents;