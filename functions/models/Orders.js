const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const shortid = require('shortid');

const  OrdersSchema = new Schema({
    name:{
        type:String,
        required:[ true,'name field is required']

    },

    username:{
        type:String,
        required:[ true,'username field is required']

    },
totalprice:{
        type:Number,
        required:[ true,'totalprice field is required']

    },

    color:{
        type:String,
        required:[ true,'color field is required']

    },

    city:{
        type:String,
   

    },
    size:{
        type:String,
        required:[ true,'size field is required']

    },
    country:{
        type:String,
        required:[ true,'country field is required']

    },


    ordernumber:{
        type:String,
        default:shortid.generate(),
        unique:true,
        required:[ true,'number field is required']

    },

    pep:{
        type:String,

    },
    status:{
        type:String,
        default:'0',

    },

    created_at: {type: Date, default: Date.now},


});

;


const Orders = mongoose.model('Orders',OrdersSchema);
module.exports=Orders;