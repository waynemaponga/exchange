const mongoose = require('mongoose');
const Schema = mongoose.Schema;
//create user schema
SALT_WORK_FACTOR = 10;
const  PasswordSchema = new Schema({
    name:{
        type:String,
        required:['Name field is required']

    },
    password:{
        type:String,
        required:['password field is required']

    },

    notes:{
        type:String,
        required:[true,'notes field is required']
    },

});



const Password = mongoose.model('password',PasswordSchema);
module.exports=Password;