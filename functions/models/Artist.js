const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const shortid = require('shortid');
//create user schema
SALT_WORK_FACTOR = 10;
const  ArtistSchema = new Schema({
    name:{
        type:String,
    
        required:['Name field is required']

    },   user:{
        type:String,

    
        required:['user field is required']

    },
    ID:{
        type:String,
        unique:true,
        'default': shortid.generate,
        required:[ true,'Name field is required']

    },
    booknumber:{
        type:String,
  
        required:['booknumber field is required']

    },

    pic:{
        type:String,
  
        required:['pic field is required']

    },
    facebook:{
        type:String,
  
        required:['facebook field is required']

    },
     youtube:{
        type:String,
  
        required:['youtube field is required']

    },

    instagram:{
        type:String,
  
        required:['facebook field is required']

    },



});



const Artist = mongoose.model('Artist',ArtistSchema);
module.exports=Artist;