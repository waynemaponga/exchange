const functions = require('firebase-functions');
const express = require('express');
const bodyPaser = require('body-parser');
const mongoose = require('mongoose');
const app = express();
const routes = require('./routes/routing');
const Cloud = require('@google-cloud/storage');
const path = require('path');
nev = require('email-verification')(mongoose);
var cors = require('cors')
app.use(cors())
app.use(bodyPaser.json());
//init routes
app.use(routes);

//connect to mongodb
const serviceKey = path.join(__dirname, './keys.json');
const { Storage } = Cloud
const storage = new Storage({
  keyFilename: serviceKey,
  projectId: 'exchnage-23229',
})
mongoose.connect("mongodb://munashe:munashe95@cluster0-shard-00-00.nm901.mongodb.net:27017,cluster0-shard-00-01.nm901.mongodb.net:27017,cluster0-shard-00-02.nm901.mongodb.net:27017/PExchange?ssl=true&replicaSet=Cluster0-shard-0&authSource=admin&retryWrites=true&w=majority");
mongoose.Promise = global.Promise;

app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});

//error handling
app.use(function (err,req,res,next) {

     console.log(err);
    //console.log(err.message);
    if(err.message.search('username_1')){
        res.status(200).send({message:err});
    
    }

    if(err){
        res.status(409).send({message:err});
    
    }
    // else{
    //     res.status(422).send({error:err.message});
    //
    // }
});

app.get('/test'),(req,res,next)=>{
    res.send('hello')
}
 
app.listen(process.env.port||5000,function () {

    console.log("now listening "+process.env.port);

});
module.exports = storage;
exports.app = functions.https.onRequest(app);
